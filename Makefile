diff: diff.o stack.o
	cc diff.o stack.o -o project

diff.o: stack.h stack.c diff.c
	cc -c diff.c

stack.o: stack.h stack.c
	cc -c stack.c