#include <stdio.h>
#include <stdlib.h>


typedef struct nums {
    int i,j;
}nums; 

typedef struct node { 
	nums nums;
	struct node *next;
}node;

typedef struct stack {
	node *head, *tail;
	int stacksize;
}stack;

void stackinit(stack *q);
int isempty(stack *q);
int isfull(stack *q);
void push(stack *q, nums d);
nums pop(stack *q);
int stacksize(stack q);
