diff Usage : ./project file1 file2 

patch Usage : ./project file1 patchfile

For the diff command my code reads the two files given line-by-line and stores these lines in arrays of character pointers.
These arrays form a table as given in the book "Introduction to Algorithms". The number of rows and columns are equal to the number of lines in the first file and second file respectively.
Then two tables of the same size as above are generated, num table - one stores numbers and dir table - the other directions for traversal of the table of lines
read from the files.
Using the Longest common subsequence algorithm the numbers are filled in the 'num' table and traversal directions are feed into the 'dir' table.
During traversal we have three directions to move. N - up, W - left, NW - diagonally up.
Depending upon the direction we move deletion, addition or change of text is determined. 
N means text is to be deleted.
W means text is to be added.
And NW means text is to be changed.
Then while traversing using the 'dir' table if we encounter a N the lines and there position in respective files is stored in a stack
called delstack.
If we encounter a W the lines and there position in respective files is stored in a stack called addstack.
And whenever we encounter a NW the stacks are emptied to produce the desired output.